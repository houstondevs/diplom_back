from django.urls import path

from app_accounts.views import registration_view, MyTokenObtainPairView, UserProfileView

from courses.views import CoursesViewSet

from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)


router = DefaultRouter()
router.register(r'courses', CoursesViewSet, basename='courses')

urlpatterns = [
    path('registration/', registration_view, name='registration_view'),
    path('token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('me/', UserProfileView.as_view(), name="profile_view"),
]

urlpatterns += router.urls