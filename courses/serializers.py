from rest_framework import serializers
from rest_framework import generics

from .models import Courses


class CourseDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Courses
        fields = ['title', 'text']


class CourseListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Courses
        fields = ['title', 'pk', 'description', 'title_image']