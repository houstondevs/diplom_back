from django.contrib import admin

from .models import Courses, Image


admin.site.register(Courses)
admin.site.register(Image)