from django.shortcuts import render

from rest_framework import viewsets

from .serializers import CourseDetailSerializer, CourseListSerializer
from .models import Courses


class CoursesViewSet(viewsets.ModelViewSet):
    queryset = Courses.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return CourseDetailSerializer
        return CourseListSerializer
