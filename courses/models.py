from django.db import models


class Courses(models.Model):
    class Meta:
        verbose_name = "Курс"
        verbose_name_plural = "Курсы"

    title = models.CharField(max_length=20)
    title_image = models.ImageField(upload_to='TitleImages/', blank=False, null=False, default='default.png')
    text = models.TextField(max_length=10000)
    description = models.CharField(max_length=200)


class Image(models.Model):
    image = models.ImageField(upload_to='courseImages/')