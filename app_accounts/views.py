from django.contrib.auth import get_user_model
from rest_framework_simplejwt.views import TokenObtainPairView

from .serializers import RegistrationSerializer, MyTokenObtainPairSerializer, ProfileSerializer

from rest_framework import (decorators, permissions, response, status, generics)


User = get_user_model()


#{"phone_number":"832432","password":"qwertyuiop"}
@decorators.api_view(['POST',])
@decorators.permission_classes((permissions.AllowAny,))
def registration_view(request):
    serializer = RegistrationSerializer(data=request.data)
    if serializer.is_valid():
        User.objects.create_user(phone_number=serializer.data['phone_number'], password=serializer.data['password'])
        return response.Response({'detail':"Успешная регистрация!",}, status=status.HTTP_201_CREATED)
    return response.Response({'detail': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class UserProfileView(generics.RetrieveUpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ProfileSerializer
    queryset = User.objects.all()

    def get_object(self):
        return self.request.user


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


