from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.conf import settings
from django.core.validators import RegexValidator


class UserManager(BaseUserManager):
    def create_user(self, phone_number, password, **extra_fields):
        if not phone_number:
            raise ValueError("Укажите номер телефона!")
        user = self.model(phone_number=phone_number, **extra_fields)
        user.set_password(password)
        user.save()

    def create_superuser(self, phone_number, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        if extra_fields.get('is_active') is not True:
            raise ValueError('Superuser must have is_active=True')
        return self.create_user(phone_number, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Номер телефона должен быть в формате: '+999999999'.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, unique=True)
    image = models.ImageField(verbose_name="Аватар", blank=True, null=True, upload_to='avatars/')
    first_name = models.CharField(verbose_name="Имя", max_length=20, blank=True, null=True)
    middle_name = models.CharField(verbose_name="Отчество", max_length=20, blank=True, null=True)
    last_name = models.CharField(verbose_name="Фамилия", max_length=20, blank=True, null=True)
    email = models.EmailField(verbose_name="Почтовый адрес", max_length=50, blank=True, null=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=settings.IS_ACTIVE)
    date_joined = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'phone_number'

    objects = UserManager()

    def __str__(self):
        return self.phone_number
